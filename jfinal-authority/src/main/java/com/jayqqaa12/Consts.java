package com.jayqqaa12;

public class Consts
{

	/***
	 * shiro 权限 管理 开关
	 */
	public static boolean OPEN_SHIRO = true;

	/***
	 * 分布式session开关 请在redis.properties 配置ip和端口
	 */
	public static boolean OPEN_REDIS = true;


}
