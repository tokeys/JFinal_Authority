package com.jfinal.ext.plugin.cim;

import java.util.HashMap;

import com.farsunset.cim.nio.constant.CIMConstant;
import com.farsunset.cim.nio.handle.CIMRequestHandler;
import com.farsunset.cim.nio.handle.MainIOHandler;
import com.jfinal.plugin.IPlugin;

public class CIMPlugin implements IPlugin 
{
	private HashMap<String, CIMRequestHandler> handlers= new HashMap<String, CIMRequestHandler>();
	private int port =23456;
	
	
	public void setLogoutHandler( CIMRequestHandler handler){
		handlers.put(CIMConstant.RequestKey.CLIENT_LOGOUT, handler);
	}
	public void setBindHandler( CIMRequestHandler handler){
		handlers.put(CIMConstant.RequestKey.CLIENT_BIND, handler);
	}
	public void setHeartBeatHandler( CIMRequestHandler handler){
		handlers.put(CIMConstant.RequestKey.CLIENT_HEARTBEAT, handler);
	}
	public void setSessionClosedHandler( CIMRequestHandler handler){
		handlers.put("sessionClosedHander", handler);
	}
	
	
	public void setHandler(HashMap<String, CIMRequestHandler> handlers){
		this.handlers= handlers;
	}
	
	public void setPort(int port){
		
		this.port = port;
	}
	

	@Override
	public boolean start()
	{
		
		MainIOHandler main=new MainIOHandler();
		main.setPort(port);
		main.setHandlers(handlers);
		main.init();
		
		return true;
	}

	@Override
	public boolean stop()
	{
		return true;
	}

}
